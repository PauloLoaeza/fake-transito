package fake.transito

import grails.rest.*

@Resource(uri="/infraccion")
class Infraccion {

    Date dateCreated
    Integer status
    String nombre
    String descripcion
    Double precio

    Vehiculo vehiculo
    Transito transito

    static belongsTo = [persona: Persona]

    static constraints = {
    }
}
