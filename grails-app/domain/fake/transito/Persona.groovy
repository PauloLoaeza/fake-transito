package fake.transito
import grails.rest.*

@Resource(uri="/persona")
class Persona {

    String nombre
    String curp
    String numLicencia
    Date vigenciaLicencia
    String tipoSangre
    String numEmergencia

    static hasMany = [infracciones: Infraccion]

    static constraints = {
    }
}
