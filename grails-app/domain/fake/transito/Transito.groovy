package fake.transito
import grails.rest.*

@Resource(uri="/transito")
class Transito {

    String nombre
    String curp
    String correo
    String password

    static constraints = {
    }
}
