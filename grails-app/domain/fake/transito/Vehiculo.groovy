package fake.transito
import grails.rest.*

@Resource(uri="/vehiculo")
class Vehiculo {

    String modelo
    String marca

    static constraints = {
    }
}
